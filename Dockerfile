FROM php:7.4-cli

RUN apt-get update

RUN apt-get install -y \
    unzip \
    libzip-dev \
    zip \
    git

COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

WORKDIR /

COPY . /

RUN composer install

#CMD [ "php", "src/index.php", "app:generate-webpage-command" ]
