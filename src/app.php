<?php

require __DIR__ . '/../vendor/autoload.php';

use Symfony\Component\Console\Application;
use App\Command\GenerateWebPageJSONCommand;

$application = new Application;
$application->add(new GenerateWebPageJSONCommand);
$application->run();
