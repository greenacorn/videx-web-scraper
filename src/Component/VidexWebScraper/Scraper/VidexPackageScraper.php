<?php

namespace App\Component\VidexWebScraper\Scraper;

use App\Component\WebScraper;
use Symfony\Component\DomCrawler\Crawler;
use App\Component\VidexWebScraper\Filter\VidexPackageFilter;
use App\Component\VidexWebScraper\DataObject\VidexPackage;

class VidexPackageScraper extends WebScraper
{
    /**
     * @return array
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function scrape(): array
    {
        $packages = [];
        foreach ($this->fetchPackages() as $domElement) {
            $filter = new VidexPackageFilter(new Crawler($domElement));

            $package = new VidexPackage;
            $package->setOptionTitle($filter->getTitle());
            $package->setDescription($filter->getDescription());
            $package->setPrice($filter->getPrice());
            $package->setDiscount($filter->getDiscount());

            $packages[] = $package->toArray();
        }

        usort($packages, fn ($a, $b) => $a['price'] > $b['price']);
        return $packages;
    }

    /**
     * @return Crawler
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    private function fetchPackages(): Crawler
    {
        return $this->fetch()->filter('div > .package');
    }
}
