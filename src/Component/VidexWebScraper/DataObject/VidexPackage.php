<?php

namespace App\Component\VidexWebScraper\DataObject;

class VidexPackage
{
    private $optionTitle;

    private $description;

    private $price;

    private $discount;

    public function getOptionTitle(): ?string
    {
        return $this->optionTitle;
    }

    public function setOptionTitle(string $optionTitle): self
    {
        $this->optionTitle = $optionTitle;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getDiscount(): ?int
    {
        return $this->discount;
    }

    public function setDiscount(?int $discount): self
    {
        $this->discount = $discount;

        return $this;
    }

    public function toArray(): array
    {
        return [
            'price' => $this->getPrice(),
            'discount' => $this->getDiscount(),
            'optionTitle' => $this->getOptionTitle(),
            'description' => $this->getDescription(),
        ];
    }
}
