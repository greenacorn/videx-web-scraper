<?php

namespace App\Component\VidexWebScraper\Filter;

use Symfony\Component\DomCrawler\Crawler;

class VidexPackageFilter
{
    private Crawler $htmlDom;

    public function __construct(Crawler $htmlDom)
    {
        $this->htmlDom = $htmlDom;
    }

    public function getTitle(): string
    {
        return $this->htmlDom->filter('div.header h3')->text();
    }

    public function getDescription(): string
    {
        return $this->htmlDom->filter('div.package-name')->text();
    }

    public function packagePrices(): Crawler
    {
        return $this->htmlDom->filter('div.package-price')->children();
    }

    public function getPrice(): int
    {
        $price = $this->packagePrices()->first()->text();
        return (int)(preg_replace('/£/', '', $price) ?? 0) * 100;
    }

    public function getDiscount(): int
    {
        $discount = $this->packagePrices()->last()->text();
        preg_match('/Save £(?P<price>[0-9.]+)/', $discount, $array);
        return (int)($array['price'] ?? 0) * 100;
    }
}
