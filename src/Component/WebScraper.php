<?php

namespace App\Component;

use Symfony\Component\DomCrawler\Crawler;
use Symfony\Contracts\HttpClient\HttpClientInterface;

abstract class WebScraper
{
    private ?string $url;

    private HttpClientInterface $client;

    /**
     * VidexWebScraper constructor.
     * @param HttpClientInterface $client
     * @param string $url
     */
    public function __construct(HttpClientInterface $client, string $url = null)
    {
        $this->url = $url;
        $this->client = $client;
    }

    abstract public function scrape(): array;

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @return Crawler
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    protected function fetch(): Crawler
    {
        $content = $this->client->request('GET', $this->url)->getContent();

        return new Crawler($content);
    }
}
