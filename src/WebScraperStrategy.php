<?php

namespace App;

use Exception;
use App\Component\WebScraper;
use Symfony\Component\HttpClient\CurlHttpClient;
use App\Component\VidexWebScraper\Scraper\VidexPackageScraper;

class WebScraperStrategy
{
    private const STRATEGY_VIDEX_PACKAGE = 'VidexWebScraper::VidexPackageScraper';

    private WebScraper $strategy;

    /**
     * WebScraperStrategy constructor.
     * @param string $strategy
     * @throws Exception
     */
    public function __construct($strategy = self::STRATEGY_VIDEX_PACKAGE)
    {
        $this->changeStrategy($strategy);
    }

    /**
     * @return WebScraper
     */
    public function getStrategy(): WebScraper
    {
        return $this->strategy;
    }

    /**
     * @param string $newStrategy
     * @throws Exception
     */
    public function changeStrategy(string $newStrategy): void
    {
        switch ($newStrategy) {
            case static::STRATEGY_VIDEX_PACKAGE:
                $this->strategy = new VidexPackageScraper(new CurlHttpClient);
                break;
            default:
                throw new Exception("{$newStrategy} is not a valid strategy.");
        }
    }
}
