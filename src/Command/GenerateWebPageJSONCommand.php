<?php

namespace App\Command;

use App\WebScraperStrategy;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GenerateWebPageJSONCommand extends Command
{
    private const JSON_FILE = 'var/page.json';

    protected static $defaultName = 'app:generate-webpage-json';

    protected function configure()
    {
        $this
            ->setDescription('Generate Web Page JSON')
            ->addArgument('url', InputArgument::REQUIRED, 'URL of the web page to scrape')
            ->addArgument(
                'strategy',
                InputArgument::OPTIONAL,
                'Web scraper strategy',
                'VidexWebScraper::VidexPackageScraper'
            );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $url = $input->getArgument('url');

        if (filter_var($url, FILTER_VALIDATE_URL)) {
            $io->note(sprintf('Scraping URL: %s', $url));

            $strategy = (new WebScraperStrategy($input->getArgument('strategy')))->getStrategy()->setUrl($url);

            file_put_contents(__DIR__ . '/../../' . static::JSON_FILE, json_encode($strategy->scrape()));

            $io->success('Json File is in ~/' . static::JSON_FILE);
            return;
        }

        $io->error(sprintf('%s is not a valid URL', $url));
    }
}
