## Web scraper with example strategy to export packages on https://videx.comesconnected.com to a JSON file

#### Technology
- PHP 7.4
- Docker (php:7.4-cli)
- Symfony Console
- Symfony HTTPClient
- Symfony DomCrawler
- Symfony Css Selector
- PHPUnit

### Usage

##### 1. Build the Docker container & install dependencies
```
docker-compose build
```

##### 2 Run the Videx Scraper

```
docker-compose run app php src/app.php app:generate-webpage-json https://videx.comesconnected.com/
```

###### 2.1 View JSON file
View of saved JSON file containing product data in a ascending price order 

```
docker-compose run app cat var/page.json
```

##### 3 Run tests
```
docker-compose run app vendor/bin/phpunit --configuration phpunit.xml
```
