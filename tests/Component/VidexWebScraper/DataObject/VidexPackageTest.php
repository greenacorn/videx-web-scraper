<?php

namespace Test\Component\idexWebScraper\Filter;

use PHPUnit\Framework\TestCase;
use App\Component\VidexWebScraper\DataObject\VidexPackage;

class VidexPackageTest extends TestCase
{
    private VidexPackage $package;

    public function setUp(): void
    {
        $this->package = new VidexPackage;
    }

    public function testPrice()
    {
        $this->package->setPrice(100);
        $this->assertEquals(100, $this->package->getPrice());
    }

    public function testDescription()
    {
        $this->package->setDescription('TEST DESC');
        $this->assertEquals('TEST DESC', $this->package->getDescription());
    }

    public function testDiscount()
    {
        $this->package->setDiscount(1);
        $this->assertEquals(1, $this->package->getDiscount());
    }

    public function testOptionTitle()
    {
        $this->package->setOptionTitle('TEST TITLE');
        $this->assertEquals('TEST TITLE', $this->package->getOptionTitle());
    }
}
