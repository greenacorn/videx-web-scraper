<?php

namespace Test\Component\idexWebScraper\Filter;

use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\HttpClient\Response\MockResponse;
use App\Component\VidexWebScraper\Scraper\VidexPackageScraper;

class VidexPackageScraperTest extends TestCase
{
    public function testScrape()
    {
        $client = new MockHttpClient([new MockResponse($this->getMockHTML())]);
        $scraper = new VidexPackageScraper($client, 'http://test.com');

        $this->assertEquals($scraper->scrape(), [
            [
                'price' => 10800,
                'discount' => 1200,
                'optionTitle' => 'Option 2000 Mins',
                'description' => 'Up to 2000 minutes talk time per year including 420 SMS(5p / minute and 4p / SMS thereafter)'
            ]
        ]);
    }

    private function getMockHTML()
    {
        return <<< 'HTML'
<!DOCTYPE html>
<html>
    <body>
        <div class="col-xs-4">
            <div class="package featured center" style="margin-left:0px;">
                <div class="header dark-bg">
                    <h3>Option 2000 Mins</h3>
                </div>
                <div class="package-features">
                    <ul>
                        <li>
                            <div class="package-name">Up to 2000 minutes talk time per year<br> including 420 SMS<br>(5p / minute and 4p / SMS thereafter)</div>
                        </li>
                        <li>
                            <div class="package-price"><span class="price-big">£108.00</span><br>(inc. VAT)<br>Per Year
                               <p style="color: red">Save £12 on the monthly price</p>
                            </div>
                        </li>
                        <li>
                            <div class="package-data">12 Months - Voice &amp; SMS Service Only</div>
                        </li>
                    </ul>
                    <div class="bottom-row">
                        <a class="btn btn-primary main-action-button" href="activate" role="button">Choose</a>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
HTML;
    }
}
