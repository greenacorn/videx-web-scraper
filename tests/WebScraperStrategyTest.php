<?php

namespace Test\Component\idexWebScraper\Filter;

use Exception;
use App\WebScraperStrategy;
use PHPUnit\Framework\TestCase;
use App\Component\VidexWebScraper\Scraper\VidexPackageScraper;

class WebScraperStrategyTest extends TestCase
{
    public function testDefaultStrategy()
    {
        $strategy = (new WebScraperStrategy())->getStrategy();
        $this->assertInstanceOf(VidexPackageScraper::class, $strategy);
    }

    public function testInvalidStrategy()
    {
        $this->expectException(Exception::class);
        new WebScraperStrategy('BadStrategy');
    }
}
